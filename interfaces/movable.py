""" Interface of movable object """


class Movable:
    """ Interface of moving object """
    def set_position(self, new_position):
        """ set position of object """

    def get_position(self):
        """ return position of object """

    def get_velocity(self):
        """ return position of object """
