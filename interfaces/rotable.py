""" Interface of rotating object """


class Rotable:
    """ Interface of rotating object """
    def set_direction(self, new_direction):
        """ set direction of object """

    def get_direction(self):
        """ return direction of object """

    def get_directions_number(self):
        """ return directions number """

    def get_angular_velocity(self):
        """ return angular velocity of object """