class VelocityChangeable:
    """ Interface for change velocity according to rotation """
    def get_angle(self):
        """ return angle of object """

    def get_velocity_value(self):
        """ return position of object """

    def set_velocity(self, new_velocity):
        """ set velocity of object """