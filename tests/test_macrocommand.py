import unittest
from unittest.mock import Mock
import commands
from exceptions.exceptions import CommandException
from vector2d.vector2d import Vector2D


class MacroCommandTests(unittest.TestCase):
    def test_three_identical_commands(self):
        movable_mock = Mock()
        movable_mock.get_position.return_value = Vector2D(12, 5)
        movable_mock.get_velocity.return_value = Vector2D(-7, 3)

        cmd = commands.MacroCommand((commands.Move(movable_mock),
                              commands.Move(movable_mock),
                              commands.Move(movable_mock))
                              )
        cmd.execute()
        self.assertEqual(3, movable_mock.set_position.call_count)

        movable_mock.set_position.assert_called_with(Vector2D(5, 8))

    def test_moving_in_straight_line(self):
        """ Движение по прямой с проверкой топлива """
        uobject_mock = Mock()

        uobject_mock.get_position.return_value = Vector2D(12, 5)
        uobject_mock.get_velocity.return_value = Vector2D(-7, 3)

        uobject_mock.get_fuellevel.return_value = 6
        uobject_mock.get_fuelflow.return_value = 2

        cmd = commands.MacroCommand((commands.CheckFuel(uobject_mock),
                              commands.Move(uobject_mock),
                              commands.BurnFuel(uobject_mock))
                              )
        cmd.execute()

        uobject_mock.set_position.assert_called_once_with(Vector2D(5, 8))
        uobject_mock.set_fuellevel.assert_called_once_with(4)

    def test_break_execute_when_exception(self):
        """ Движение по прямой с проверкой топлива когда топлива не хватило"""
        uobject_mock = Mock()

        uobject_mock.get_position.return_value = Vector2D(12, 5)
        uobject_mock.get_velocity.return_value = Vector2D(-7, 3)

        uobject_mock.get_fuellevel.return_value = 2
        uobject_mock.get_fuelflow.return_value = 6

        cmd = commands.MacroCommand((commands.CheckFuel(uobject_mock),
                              commands.Move(uobject_mock),
                              commands.BurnFuel(uobject_mock))
                              )

        with self.assertRaises(CommandException):
            cmd.execute()

        uobject_mock.set_position.assert_not_called()
        uobject_mock.set_fuellevel.assert_not_called()

    def test_rotate_moving_object(self):
        uobject_mock = Mock()


        uobject_mock.get_position.return_value = Vector2D(12, 5)
        uobject_mock.get_velocity.return_value = Vector2D(-2, -2)

        # direction befor rotating 1, after rotating 5.
        uobject_mock.get_direction.return_value = 1
        uobject_mock.get_angular_velocity.return_value = -4
        uobject_mock.get_directions_number.return_value = 8

        # direction 5 (225 deg)
        uobject_mock.get_angle.return_value = 225
        uobject_mock.get_velocity_value.return_value = 2.83

        cmd = commands.MacroCommand((commands.Rotate(uobject_mock),
                              commands.ChangeVelocity(uobject_mock),
                              commands.Move(uobject_mock))
                              )

        cmd.execute()

        uobject_mock.set_direction.assert_called_with(5)
        uobject_mock.set_velocity.assert_called_with((-2, -2))
        uobject_mock.set_position.assert_called_with(Vector2D(10, 3))
