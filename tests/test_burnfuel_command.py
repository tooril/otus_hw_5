import unittest
from unittest.mock import Mock
from commands.burnfuel import BurnFuel



class BurnFuelTests(unittest.TestCase):
    def test_burn_fuel(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 6
        fuelable_mock.get_fuelflow.return_value = 2

        cmd = BurnFuel(fuelable_mock)
        cmd.execute()

        fuelable_mock.set_fuellevel.assert_called_with(4)

    def test_does_raise_when_no_get_fuellevel_method(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.side_effect = Exception
        fuelable_mock.get_fuelflow.return_value = 2

        cmd = BurnFuel(fuelable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_does_raise_when_no_get_fuelflow_method(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 6
        fuelable_mock.get_fuelflow.side_effect = Exception

        cmd = BurnFuel(fuelable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_does_raise_when_no_get_set_fuellevel_method(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 6
        fuelable_mock.get_fuelflow.return_value = 2

        fuelable_mock.set_fuellevel.side_effect = Exception

        cmd = BurnFuel(fuelable_mock)

        with self.assertRaises(Exception):
            cmd.execute()