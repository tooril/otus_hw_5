from unittest.mock import Mock
import unittest
from vector2d.vector2d import Vector2D
from commands.changevelocity import ChangeVelocity


class ChangeVelocityTest(unittest.TestCase):
    def test_change_velocity_with_rotation(self):
        velocity_changeable_mock = Mock()

        # direction 5 (225 deg)
        velocity_changeable_mock.get_angle.return_value = 225
        velocity_changeable_mock.get_velocity_value.return_value = 2.83

        cmd = ChangeVelocity(velocity_changeable_mock)

        cmd.execute()

        velocity_changeable_mock.set_velocity.assert_called_with((-2, -2))

    def test_does_raise_when_no_get_angle_method(self):
        velocity_changeable_mock = Mock()

        velocity_changeable_mock.get_angle.side_effect = Exception
        velocity_changeable_mock.get_velocity_value.return_value = 2.83

        cmd = ChangeVelocity(velocity_changeable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_does_raise_when_no_get_velocity_value_method(self):
        velocity_changeable_mock = Mock()

        velocity_changeable_mock.get_angle.return_value = 225
        velocity_changeable_mock.get_velocity_value.side_effect = Exception

        cmd = ChangeVelocity(velocity_changeable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_does_raise_when_no_set_velocity_method(self):
        velocity_changeable_mock = Mock()

        velocity_changeable_mock.get_angle.return_value = 225
        velocity_changeable_mock.get_velocity_value.return_value = 2.83

        velocity_changeable_mock.set_velocity.side_effect = Exception

        cmd = ChangeVelocity(velocity_changeable_mock)

        with self.assertRaises(Exception):
            cmd.execute()
