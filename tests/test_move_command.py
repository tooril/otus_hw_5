from unittest.mock import Mock
import unittest
from vector2d.vector2d import Vector2D
from commands.move import Move



class MovingTests(unittest.TestCase):
    def test_set_position(self):
        movable_mock = Mock()
        movable_mock.get_position.return_value = Vector2D(12, 5)
        movable_mock.get_velocity.return_value = Vector2D(-7, 3)

        cmd = Move(movable_mock)
        cmd.execute()

        movable_mock.set_position.assert_called_with(Vector2D(5, 8))

    def test_try_get_position_exeption(self):
        """ try to read position when unable to read """
        movable_mock = Mock()
        movable_mock.get_position.side_effect = Exception
        movable_mock.get_velocity.return_value = Vector2D(-7, 3)

        cmd = Move(movable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_try_get_velocity_exeption(self):
        """ try to read velocity when unable to read """
        movable_mock = Mock()
        movable_mock.get_position.return_value = Vector2D(12, 5)
        movable_mock.get_velocity.side_effect = Exception

        cmd = Move(movable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_try_set_position_exeption(self):
        """ try to write position when unable to write """
        movable_mock = Mock()
        movable_mock.get_position.return_value = Vector2D(12, 5)
        movable_mock.get_velocity.return_value = Vector2D(-7, 3)

        movable_mock.set_position.side_effect = Exception

        cmd = Move(movable_mock)

        with self.assertRaises(Exception):
            cmd.execute()
