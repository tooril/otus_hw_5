import unittest
from unittest.mock import Mock
from commands.checkfuel import CheckFuel
from exceptions.exceptions import CommandException


class CheckFuelTests(unittest.TestCase):
    def test_does_not_raise_when_fuel_isenough(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 6
        fuelable_mock.get_fuelflow.return_value = 2

        cmd = CheckFuel(fuelable_mock)

        raised = False
        try:
            cmd.execute()
        except:
            raised = True

        self.assertFalse(raised)

    def test_does_raise_when_fuel_is_not_enough(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 2
        fuelable_mock.get_fuelflow.return_value = 6

        cmd = CheckFuel(fuelable_mock)

        with self.assertRaises(CommandException):
            cmd.execute()

    def test_does_raise_when_no_get_fuellevel_method(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.side_effect = Exception
        fuelable_mock.get_fuelflow.return_value = 2

        cmd = CheckFuel(fuelable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_does_raise_when_no_get_fuelflow_method(self):
        fuelable_mock = Mock()
        fuelable_mock.get_fuellevel.return_value = 6
        fuelable_mock.get_fuelflow.side_effect = Exception

        cmd = CheckFuel(fuelable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

