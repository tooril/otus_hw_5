from typing import Union
from interfaces.command import Command


ListCommands = list[Command]
TupleCommands = tuple[Command]


class MacroCommand(Command):
    """ MacroCommand """
    def __init__(self, commands: Union[ListCommands, TupleCommands]):
        self.commands = commands

    def execute(self):
        """ executes commands in a loop """
        for cmd in self.commands:
            cmd.execute()
