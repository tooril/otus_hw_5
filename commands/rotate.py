from interfaces.command import Command
from interfaces.rotable import Rotable

class Rotate(Command):
    """ Executes a rotation of object """
    def __init__(self, obj: Rotable):
        self.obj = obj

    def execute(self):
        """ executes a rotating """
        self.obj.set_direction((self.obj.get_direction()
                               + self.obj.get_angular_velocity())
                               % self.obj.get_directions_number())