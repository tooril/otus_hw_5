from interfaces.command import Command
from interfaces.logable import Logable

class Log(Command):
    """ Log command """
    def __init__(self, obj: Logable):
        self.obj = obj

    def execute(self):
        """ executes logging """
        self.obj.log()