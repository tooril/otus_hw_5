from interfaces.command import Command
from interfaces.fuelable import Fuelable

class BurnFuel(Command):
    """ Burn fuel command """
    def __init__(self, obj: Fuelable):
        self.obj = obj

    def execute(self):
        """ Executes fuel burning """
        self.obj.set_fuellevel(
                self.obj.get_fuellevel() - self.obj.get_fuelflow())