from interfaces.command import Command
from interfaces.movable import Movable


class Move(Command):
    """ Executes a move of object """
    def __init__(self, obj: Movable):
        self.obj = obj

    def execute(self):
        """ executes a moving """
        self.obj.set_position(self.obj.get_position() +
                              self.obj.get_velocity())