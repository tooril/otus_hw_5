from interfaces.command import Command
from interfaces.fuelable import Fuelable
from exceptions.exceptions import CommandException


class CheckFuel(Command):
    """ This Command checks the fuel  level"""
    def __init__(self, obj: Fuelable):
        self.obj = obj

    def execute(self):
        if self.obj.get_fuellevel() - self.obj.get_fuelflow() < 0:
            raise CommandException
