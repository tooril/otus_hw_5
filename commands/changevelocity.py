""" Commad for change vector of velocity according with angle """
import math
from interfaces.command import Command
from interfaces.velocity_changeable import VelocityChangeable

class ChangeVelocity(Command):
    """ Command changes the velocity """
    def __init__(self, obj: VelocityChangeable):
        self.obj = obj

    def execute(self):
        """ Execution changes the velocity according to rotation """
        velocity_val = self.obj.get_velocity_value()
        angel = self.obj.get_angle()
        rad_angle = math.radians(angel)

        self.obj.set_velocity((int(velocity_val * math.cos(rad_angle)),
                               int(velocity_val * math.sin(rad_angle)))
                              )